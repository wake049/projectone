/*creating all tables*/
create table reimbursment(
	remib_Id int primary key generated always as identity,
	remib_amount numeric,
	remib_submitted date,
	remib_resolved date,
	remib_description varchar(250),
	remib_author int references users(userId) not null,
	remib_resolver int references users(userId),
	remib_statusId int references remibstatus(remib_statusId) not null,
	remib_typeId int references remibtype(remibtypeId) not null
);

create table users(
	userId int primary key generated always as identity,
	username varchar(50),
	password varchar(50),
	firstname varchar(100),
	lastname varchar(100),
	email varchar(150),
	userRoleId int references userRoles(userRolesId) not null
);

create table remibStatus(
	remib_statusId int primary key generated always as identity,
	remib_status varchar(10)	
);
create table remibType(
	remibtypeId int primary key generated always as identity,
	remib_type varchar(20)
);
create table userRoles(
	userRolesId int primary key generated always as identity,
	userRole varchar(30)
);

/*creating functions*/
create or replace function insert_remib(iremib_amount numeric, iremib_submitted date, iremib_resolved date, iremib_description varchar(250), iauthor int, iresolver int,
												istatusId int, itypeId int)
returns varchar(10) as $$
begin 
	insert into reimbursment(remib_amount, remib_submitted, remib_resolved, remib_description, 
		remib_author, remib_resolver, remib_statusId, remib_typeId) values(iremib_amount, iremib_submitted, iremib_resolved, iremib_description, iauthor, iresolver, istatusId, itypeId);
return 'success';
end
$$ language 'plpgsql';

create or replace function insert_remibless(iremib_amount numeric, iremib_submitted date, iremib_description varchar(250), iauthor int,
												istatusId int, itypeId int)
returns varchar(10) as $$
begin 
	insert into reimbursment(remib_amount, remib_submitted, remib_description, 
		remib_author, remib_statusId, remib_typeId) values(iremib_amount, iremib_submitted, iremib_description, iauthor, istatusId, itypeId);
return 'success';
end
$$ language 'plpgsql';

create or replace function insert_user(iusername varchar(50), ipassword varchar(50), ifirstname varchar(100), ilastname varchar(100), iemail varchar(150),
iuserRoleId int)
returns varchar(10) as $$
begin 
	insert into users(username, password, firstname, lastname, email, userRoleId) values(iusername, ipassword, ifirstname, ilastname, iemail, iuserRoleId);
return 'success';
end
$$ language 'plpgsql';
 
/* enum insert tables*/ 
insert into userroles(userRole) values('Manager');
insert into userroles (userRole) values('Employee');
insert into remibStatus (remib_status) values('Pending');
insert into remibStatus (remib_status) values('Approved');
insert into remibStatus (remib_status) values('Declined');
insert into remibType(remib_type) values('Travel and Mileage');
insert into remibType(remib_type) values('Buisness Expense');
insert into remibType(remib_type) values('Healthcare expense');
insert into remibType(remib_type) values('Other expense');

/*insert main tables*/
select insert_user('wake', '1234', 'William', 'Allen', 'wakebrdtyler@yahoo.com',1);
select insert_user('employee', 'employee', 'Kevin', 'Piercall', 'employeeemail@yahoo.com', 2);
select insert_user('employee1', 'employee1', 'Zach', 'Weston', 'employeeemail@yahoo.com', 2);
select insert_remib(110.10, '1/16/22', '1/17/22', 'food expenses for buisness trip', 2, 1, 2, 2);
select insert_remib(900.99, '1/16/22', null, 'plane ticket for buisness trip', 2, null, 1, 1);
select insert_remib(300.10, '12/20/22', '1/1/22', 'food expenses for vacation', 2, 1, 3, 4);
select insert_remib(125.10, '1/16/22', null, 'food expenses for buisness trip', 3, null, 1, 2);
select insert_remib(702.99, '1/16/22', null, 'plane ticket for buisness trip', 3, null, 1, 1);
select insert_remib(150.10, '12/20/22', null, 'food expenses for vacation', 3, null, 1, 4);

/*select all from table*/
select * from users;
select * from reimbursment;
select * from remibstatus; 
select * from userroles; 

/*drop tables*/
drop table users;
drop table reimbursment; 
drop table remibtype;

/*select specfic from tables*/
select * from users where username = 'wake';


select u.firstname,  avg(remib_amount) from reimbursment inner join users u on remib_author = u.userid group by u.firstname;




select u.firstname, sum(remib_amount) from reimbursment r inner join users u on remib_author = u.userid group by u.firstname;

select minimum1.firstname, min(minimum1.total) totalmin 
from  (select u2.firstname, sum(remib_amount) as total
from reimbursment r inner join users u2 on r.remib_author = u2.userId 
group by u2.firstname) minimum1
group by minimum1.firstname order by totalmin asc limit 1;

select minimum1.firstname, max(minimum1.total) totalmax 
from  (select u2.firstname, sum(remib_amount) as total
from reimbursment r inner join users u2 on r.remib_author = u2.userId 
group by u2.firstname) minimum1
group by minimum1.firstname order by totalmax desc limit 1;

select r.remib_id, u.firstname, u.lastname, u1.firstname, u1.lastname, r.remib_amount, r.remib_submitted, r.remib_resolved, r.remib_description, r2.remib_status ,r3.remib_type from reimbursment r inner join users u on r.remib_author = u.userId full outer join users u1 on r.remib_resolver = u1.userid inner join remibstatus r2 on r.remib_statusid  = r2.remib_statusid inner join 
remibtype r3 on r.remib_typeid = r3.remibtypeid order by r.remib_id desc;



select u1.firstname, u1.lastname, r.remib_amount, r.remib_submitted, r.remib_resolved, r.remib_description, r2.remib_status , r3.remib_type from reimbursment r full outer join  users u1 on r.remib_resolver = u1.userid 
inner join remibstatus r2 on r.remib_statusid  = r2.remib_statusid inner join  remibtype r3 on r.remib_typeid = r3.remibtypeid where remib_author = 1;

update reimbursment set remib_resolved = '1/1/21', remib_statusid = 2, remib_resolver = 1 where remib_id = 2; 
