# ProjectOne



## Project Description

In this project I had to make a webiste were employees can submit a reimbursement and managers can approve or deny that reimbursement. Managers can also view a stastic page where they can view the most costly employee, employee in the middle and least costly employee. The employees can view thier reimbursements and submit new reimbusements

## Technolgies Used
Javalin 3.10.1
Jackson-Databind 2.13.0
JacksonAnnotations 2.12.5
Jackson-Datatype 2.12.6
slf4j-simple 1.7.30
postgresql 42.2.18
junit-jupiter-api 5.7.0
mockito-core 4.2.0
selenium 3.141.59
Eclipse ide
DBeaver
AWS
HTML
Javascript
Java 8

## Features

Employees can view and submit reimbursments
Managers can view, approve or deny, and view statistics on all reimbursments

