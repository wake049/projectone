window.onpageshow = function() {
	console.log("js is linked");
	getUsers();
	getSession();
}


let users;

function getUsers() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let use = JSON.parse(xhttp.responseText);
			users = use;
			populateTable(reimbursmenttable, users);
			setUpMainTable();
		}
	}

	xhttp.open("GET", "http://localhost:9028/user/session1");

	xhttp.send();
}

let session;
function getSession() {
	let xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {

		if (xhttp.readyState == 4 && xhttp.status == 200) {
			let use = JSON.parse(xhttp.responseText);
			session = use;
		}
	}

	xhttp.open("GET", "http://localhost:9028/user/session");

	xhttp.send();
}

let row;
let cell;


function populateTable(tableName, data) {
	for (let element of data) {
		row = tableName.insertRow();
		for (key in element) {
			cell = row.insertCell();
			let text = document.createTextNode(element[key]);
			cell.appendChild(text);
		}

	}
};

let mainTable = document.getElementById("reimbursmenttable");
function setUpMainTable() {

	//sets a classname and unique id to all status columns
	var temp = mainTable.getElementsByTagName('td');
	var num = 9;
	var j = 1;
	for (var i = 0; i < temp.length; i++) {
		if (i % num == 0 && i != 0) {
			temp[i].className = "Status";
			temp[i].id = j++;
			temp[i].setAttribute("name", "StatusNew");
			num = num + 9 + 2;
		}
	}
	//sets a classname and unique id to all id columns
	var num2 = 11;
	let m = 2;
	temp[11].setAttribute("name", "Id");
	temp[11].id = 2;
	for (var i = 0; i < temp.length; i++) {
		if (i % num2 == 0) {
			temp[i].setAttribute("name", "Id");
			temp[i].id = m++;
			num2 = num2 + 9 + 2;
		}
		temp[0].setAttribute("name", "Id");;
		temp[0].id = 1;
	}

	//sets a classname and unique id to all date columns
	var num1 = 7;
	var l = 1;
	for (var i = 0; i < temp.length; i++) {
		if (i % num1 == 0 && i != 0) {
			temp[i].className = "DateR";
			temp[i].setAttribute("name", "DateR");
			temp[i].id = l++;
			num1 = num1 + 9 + 2;
		}
	}
	var num1 = 3;
	var o = 1;
	for (var i = 0; i < temp.length; i++) {
		if (i % num1 == 0 && i != 0) {
			temp[i].setAttribute("name", "firstname1");
			temp[i].id = o++;
			num1 = num1 + 9 + 2;
		}
	}
	var num1 = 4;
	var p = 1;
	for (var i = 0; i < temp.length; i++) {
		if (i % num1 == 0 && i != 0) {
			temp[i].setAttribute("name", "lastname1");
			temp[i].id = p++;
			num1 = num1 + 9 + 2;
		}
	}

	var target_cell = document.getElementsByClassName("Status");
	var list = "<select name = statusOption onChange=changeResolvedDate(this)>" +
		"<option value=\"1\">Pending</option>" +
		"<option value=\"2\">Approved</option>" +
		"<option value=\"3\">Decline</option>" +
		"<\/select>";
	for (var i = 0; i < target_cell.length; i++) {
		if (target_cell[i].innerHTML == "Pending") {
			target_cell[i].innerHTML = list;
		}

	}
}

function changeResolvedDate(sel) {
	let xhttp = new XMLHttpRequest();
	var id = sel.parentElement.id
	var t = document.getElementsByClassName("DateR");
	var reimbid = document.getElementsByName("Id");
	let firstname = document.getElementsByName("firstname1");
	let lastname = document.getElementsByName("lastname1");
	
	var textId;
	var today = new Date();
	var newStatus;
	var date;
	for (var i = 0; i < t.length; i++) {
		if (id == t[i].id && t[i].id == reimbid[i].id && reimbid[i].id == firstname[i].id && firstname[i].id == lastname[i].id) {
			t[i].innerHTML = today.toLocaleDateString("en-US", { month: '2-digit', day: '2-digit', year: 'numeric' });
			textId = reimbid[i].innerHTML;
			newStatus = sel.parentElement.innerHTML
			newStatus = sel.value;
			firstname[i].innerHTML = session.firstname;
			lastname[i].innerHTML = session.lastname;
			date = today.toLocaleDateString("en-US", { month: '2-digit', day: '2-digit', year: 'numeric' });
		}
	}
	xhttp.open("POST", "/approve/session", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			// Response
			var response = this.responseText;
		}
	};
	var data = { textId, date, newStatus};
	xhttp.send(JSON.stringify(data));
};

let math;
let tag;
let bool1 = false;
let bool2 = false;
let bool3 = false;
let bool4 = false;

let avg = document.getElementById("averageTable");
let sum = document.getElementById("TotalTable");
let min = document.getElementById("minTable");
let max = document.getElementById("maxTable");
function average() {
	avg.style.display = "block";
	sum.style.display = "none";
	min.style.display = "none";
	max.style.display = "none";
	tag = null;
	math = null;
	let xhttp1 = new XMLHttpRequest();

	xhttp1.onreadystatechange = function() {

		if (xhttp1.readyState == 4 && xhttp1.status == 200 && bool1 == false) {
			let reimb = JSON.parse(xhttp1.responseText);
			math = reimb;
			populateTable(avg, math);
			tag = avg.getElementsByTagName("td");
			bool1 = true;
			bool2 = false;
			bool3 = false;
			bool4 = false;
		}
		if (tag != null) {
			for (let i = 0; i < tag.length; i++) {
				tag[i].id = i;
				if (tag[i].innerHTML == "null") {
					tag[i].remove();
				}
			}
			for (let i = 0; i < tag.length; i++) {
				tag[i].id = i;
				if (tag[i].innerHTML == "null") {
					tag[i].remove();
				}
			}
			for (let i = 0; i < tag.length; i++) {
				tag[i].id = i;
				if (tag[i].innerHTML == "null") {
					tag[i].remove();
				}
			}
		}
	}




	xhttp1.open("GET", "http://localhost:9028/average");

	xhttp1.send();

};

function Sum() {
	avg.style.display = "none";
	sum.style.display = "block";
	min.style.display = "none";
	max.style.display = "none";
	tag = null;
	math = null;
	let xhttp1 = new XMLHttpRequest();

	xhttp1.onreadystatechange = function() {

		if (xhttp1.readyState == 4 && xhttp1.status == 200 && bool2 == false) {
			let reimb = JSON.parse(xhttp1.responseText);
			math = reimb;
			populateTable(sum, math);
			tag = sum.getElementsByTagName("td");
			bool2 = true;
			bool1 = false;
			bool3 = false;
			bool4 = false;
		}
		if (tag != null) {
			for (let i = 0; i < tag.length; i++) {
				tag[i].id = i;
				if (tag[i].innerHTML == "null") {
					tag[i].remove();
				}
			}
			for (let i = 0; i < tag.length; i++) {
				tag[i].id = i;
				if (tag[i].innerHTML == "null") {
					tag[i].remove();
				}
			}
			for (let i = 0; i < tag.length; i++) {
				tag[i].id = i;
				if (tag[i].innerHTML == "null") {
					tag[i].remove();
				}
			}
		}
	}




	xhttp1.open("GET", "http://localhost:9028/sum");

	xhttp1.send();

};

function Min() {
	avg.style.display = "none";
	sum.style.display = "none";
	min.style.display = "block";
	max.style.display = "none";
	tag = null;
	math = null;
	let xhttp1 = new XMLHttpRequest();

	xhttp1.onreadystatechange = function() {

		if (xhttp1.readyState == 4 && xhttp1.status == 200 && bool3 == false) {
			let reimb = JSON.parse(xhttp1.responseText);
			math = reimb;
			console.log(math);
			let name = document.getElementById("minName");
			console.log(name);
			let amount = document.getElementById("minAmount");
			name.innerHTML = math.firstname;
			amount.innerHTML = math.amount;
			bool3 = true;
			bool2 = false;
			bool1 = false;
			bool4 = false;
		}
	}

	xhttp1.open("GET", "http://localhost:9028/min");

	xhttp1.send();
};

function Max() {
	avg.style.display = "none";
	sum.style.display = "none";
	min.style.display = "none";
	max.style.display = "block";
	tag = null;
	math = null;
	let xhttp1 = new XMLHttpRequest();

	xhttp1.onreadystatechange = function() {

		if (xhttp1.readyState == 4 && xhttp1.status == 200 && bool4 == false) {
			let reimb = JSON.parse(xhttp1.responseText);
			math = reimb;
			console.log(math);
			let name = document.getElementById("maxName");
			let amount = document.getElementById("maxAmount");

			name.innerHTML = math.firstname;
			amount.innerHTML = math.amount;
			bool4 = true;
			bool2 = false;
			bool3 = false;
			bool1 = false;
		}
	}
	xhttp1.open("GET", "http://localhost:9028/max");

	xhttp1.send();


};





