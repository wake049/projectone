package com.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ManagerSel {
		
		private WebDriver driver;
		private WebElement header;
		private WebElement username;
		private WebElement password;
		private WebElement submitButton;
		private WebElement select;
		public Select dropdown;

		public ManagerSel(WebDriver driver) {
			this.driver = driver;
			navigateTo();
			this.header = driver.findElement(By.tagName("h2"));
			this.username = driver.findElement(By.name("username"));
			this.password = driver.findElement(By.name("password"));
			this.submitButton = driver.findElement(By.name("usersubmit"));
			//this.select = driver.findElement(By.name("statusOption"));
			//dropdown = new Select(select);
		}
		
		public void navigateTo() {
			this.driver.get("http://localhost:9028/html/login.html");
		}
		

		
		public String getUsername() {
			return username.getAttribute("value");
		}

		public void setUsername(String username) {
			this.username.clear();
			this.username.sendKeys(username);
		}

		public String getPassword() {
			return password.getAttribute("value");
		}

		public void setPassword(String password) {
			this.password.clear();
			this.password.sendKeys(password);
		}

		public String getHeader() {
			return this.header.getText();
		}
		
		public void submit() {
			this.submitButton.click();
		}
}
