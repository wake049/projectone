package com.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.eclipse.jetty.server.Response;

import com.model.Users;
import com.service.UserService;

import io.javalin.http.Handler;

public class UserController {

	private UserService uServ;
	public boolean bool = false;
	public static boolean bool1 = false;
	public static Users use;
	public HttpServletRequest request;

	public UserController() {
		// TODO Auto-generated constructor stub
	}

	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}

	public final Handler POSTLOGIN = (ctx) -> {
		if (ctx.formParam("password") != null) {
			use = uServ.verifyUsernameAndPassword(ctx.formParam("username"), ctx.formParam("password"));
		}
		if (use != null) {
			ctx.sessionAttribute("currentuser", use);
			bool = true;
			if (use.getUserRole() == 1) {
				ctx.redirect("/html/managerpage.html");
			}
			if (use.getUserRole() == 2) {
				ctx.redirect("/html/employeepage.html");
			}
		} else {
			ctx.redirect("/html/badlogin.html");
		}
	};

	public Handler GETCURRSESS = (ctx) -> {
		if (bool == true) {
			use = uServ.getUsersByUsername(((Users) ctx.sessionAttribute("currentuser")).getUsername());
			bool = false;
			ctx.json(use);
		}
	};

	public Handler GETALLUSERS = (ctx) -> {
		List<Users> useList = uServ.getAll();
		ctx.json(useList);
	};
	public final Handler LOGOUT = (ctx)->{ 
		if(ctx.sessionAttribute("currentuser") != null) {
			HttpSession session = request.getSession();
			ctx.sessionAttribute("currentuser", null); 
			session.invalidate();
			use = null; 
	 }
	};

}
