package com.controllers;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.dao.ProjectTwoConnection;
import com.dao.UserDAOImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Reimbursment;
import com.model.Users;
import com.service.RemibServ;
import com.service.UserService;

import io.javalin.http.Handler;

public class RemibursementController {
	private RemibServ rServ;
	private Reimbursment r;
	ObjectMapper mapper = new ObjectMapper();

	Map<String, Object> newMap = new HashMap<>();
	public HttpServletRequest request;

	public RemibursementController() {
		// TODO Auto-generated constructor stub
	}

	public RemibursementController(RemibServ rServ) {
		super();
		this.rServ = rServ;
	}

	public final Handler GETALLREIM = (ctx) -> {
		if(ctx.sessionAttribute("currentuser") == null) {
			ctx.redirect("/html/login.html");
		}
		List<Reimbursment> remibList = rServ.getAll();
		ctx.json(remibList);
	};

	public final Handler ChangeStatus = (ctx) -> {
		Map<String, String> map = (Map<String, String>) mapper.readValue(ctx.body(), HashMap.class);
		int i = Integer.parseInt(map.get("textId"));
		int status = Integer.parseInt(map.get("newStatus"));
		rServ.changeStatus(UserController.use.getUserId(), i, status);
		ctx.redirect("/html/managerpage.html");
	};


	public final Handler GETALLREMIBBYUSER = (ctx) -> {
		List<Reimbursment> remib = new ArrayList<>();
			int id = UserController.use.getUserId();
			remib = (rServ.getAllById(id));
			ctx.json(remib);
	};
	
	public final Handler INSERTREIMB = (ctx) ->{
		Reimbursment r1 = new Reimbursment(Double.parseDouble(ctx.formParam("amount")), ctx.formParam("description"), ctx.formParam("type"));
		rServ.createremib(r1, UserController.use.getUserId());
		if(UserController.use.getUserRole() == 1)
			ctx.redirect("/html/managercreate.html");
		else if(UserController.use.getUserRole() == 2) {
			ctx.redirect("/html/employeecreate.html");
		}
	};
	
	public final Handler GETAVG = (ctx) ->{
		List<Reimbursment> r = rServ.avg();
		if(r != null)
		ctx.json(r);
	};
	
	public final Handler GETSUM = (ctx) ->{
		List<Reimbursment> r = rServ.sum();
		if(r != null)
		ctx.json(r);
	};
	
	public final Handler GETMIN = (ctx) ->{
		Reimbursment r = rServ.min();
		if(r != null)
		ctx.json(r);
	};
	
	public final Handler GETMAX = (ctx) ->{
		Reimbursment r = rServ.max();
		if(r != null)
		ctx.json(r);
	};
}
