package com.service;

import java.util.List;

import com.dao.ReimbDAOImpl;
import com.model.Reimbursment;

public class RemibServ {

	private ReimbDAOImpl rdi;
	
	public RemibServ() {
		// TODO Auto-generated constructor stub
	}

	public RemibServ(ReimbDAOImpl rdi) {
		super();
		this.rdi = rdi;
	}
	
	public List<Reimbursment> getAll() {
		return rdi.getAll();
	}
	public void changeStatus(int id, int i, int status) {
		rdi.changeStatus(id,i, status);
	}
	public List<Reimbursment> getAllById(int id){
		return rdi.getAllByUserId(id);
	}
	public void createremib(Reimbursment r, int id) {
		rdi.createReimb(r, id);
	}
	
	public List<Reimbursment> avg(){
		return rdi.average();
	}
	
	public List<Reimbursment> sum(){
		return rdi.sum();
	}
	
	public Reimbursment min() {
		return rdi.min();
	}
	
	public Reimbursment max() {
		return rdi.max();
	}
}
