package com.service;

import java.util.List;

import com.dao.UserDAOImpl;
import com.model.Users;

public class UserService {

	private UserDAOImpl udi;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}

	public UserService(UserDAOImpl udi) {
		super();
		this.udi = udi;
	}
	
	public Users verifyUsernameAndPassword(String username, String password) {
		Users use = getUsersByUsername(username);
		if(use.getPassword() != null)
			if(use.getPassword().equals(password))
				return use;
		return null;
	}
	
	public Users getUsersByUsername(String name) {
			return udi.getByUsername(name);
	}
	
	public List<Users> getAll(){
		return udi.getAllUsers();
		
	}
	
}
