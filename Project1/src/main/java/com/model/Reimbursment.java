package com.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
public class Reimbursment {
	//variables
	//private int reimburesmentId;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int id;
	@JsonInclude(JsonInclude.Include.NON_NULL) 
	private String firstname;
	@JsonInclude(JsonInclude.Include.NON_NULL)  
	private String lastname;
	private String firstname1;
	private String lastname1;
	private double amount;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateSubmitted;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dateResolved;
	private String description;
	private String status;
	private String type;
	
	public Reimbursment(int id, String firstname, String lastName, String firstname1, String lastname1, double amount, LocalDate dateSubmitted, LocalDate dateResolved, String descripton, String status, String type) {
		this.amount = amount;
		this.id = id;
		this.dateSubmitted = dateSubmitted;
		this.dateResolved = dateResolved;
		this.description = descripton;
		this.firstname = firstname;
		this.firstname1 = firstname1;
		this.lastname = lastName;
		this.lastname1 = lastname1;
		this.status = status;
		this.type = type;
	}
	
	

	public Reimbursment(int id, String status) {
		super();
		this.id = id;
		this.status = status;
	}
	
	
	

	
	public Reimbursment(String firstname, double amount) {
		super();
		this.firstname = firstname;
		this.amount = amount;
	}



	public Reimbursment(String firstname1, String lastname1, double amount, LocalDate dateSubmitted,
			LocalDate dateResolved, String description, String status, String type) {
		super();
		this.firstname1 = firstname1;
		this.lastname1 = lastname1;
		this.amount = amount;
		this.dateSubmitted = dateSubmitted;
		this.dateResolved = dateResolved;
		this.description = description;
		this.status = status;
		this.type = type;
	}
	
	



	public Reimbursment(double amount, String description, String type) {
		super();
		this.amount = amount;
		this.description = description;
		this.type = type;
	}



	public Reimbursment() {
		// TODO Auto-generated constructor stub
	}



	public int getId() { return id; }
	 



	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public LocalDate getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(LocalDate dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public LocalDate getDateResolved() {
		return dateResolved;
	}

	public void setDateResolved(LocalDate dateResolved) {
		this.dateResolved = dateResolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname1() {
		return firstname1;
	}

	public void setFirstname1(String firstname1) {
		this.firstname1 = firstname1;
	}

	public String getLastname1() {
		return lastname1;
	}

	public void setLastname1(String lastname1) {
		this.lastname1 = lastname1;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	
	
	
}
