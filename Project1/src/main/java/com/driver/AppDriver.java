package com.driver;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.controllers.RemibursementController;
import com.controllers.UserController;
import com.dao.ProjectTwoConnection;
import com.dao.ReimbDAOImpl;
import com.dao.UserDAOImpl;
import com.service.RemibServ;
import com.service.UserService;

import io.javalin.Javalin;

public class AppDriver {

	public static void main(String[] args) {

		UserController uc = new UserController(new UserService(new UserDAOImpl(new ProjectTwoConnection())));
		RemibursementController rc = new RemibursementController(new RemibServ(new ReimbDAOImpl(new ProjectTwoConnection())));
		Javalin app = Javalin.create(config -> {
			config.enableCorsForAllOrigins();
			config.addStaticFiles("/frontend");
		});

		app.start(9028);

		app.post("/user/login", uc.POSTLOGIN);
		app.post("/user/logout", uc.LOGOUT);
		
		app.get("/user/session", uc. GETCURRSESS);
		
		
		
		app.get("/employee/session", rc.GETALLREMIBBYUSER);

		app.get("/user/session1", rc.GETALLREIM);
		
		app.post("/approve/session", rc.ChangeStatus);
		app.post("/create", rc.INSERTREIMB);
		app.get("/average", rc.GETAVG);
		app.get("/sum", rc.GETSUM);
		app.get("/max", rc.GETMAX);
		app.get("/min", rc.GETMIN);
		
		
		File file = new File("src/main/resources/chromedriver.exe");
		
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		WebDriver driver = new ChromeDriver();
		driver.get("http://localhost:9028/html/login.html");
		driver.manage().window().maximize();
		

		app.exception(NullPointerException.class, (e, ctx)->{
		ctx.status(404);
		ctx.redirect("/html/badlogin.html");
		});
	}
}
