package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProjectTwoConnection {

	private static final String URL = "jdbc:postgresql://dec-2112-wta-db.codhbolqpji2.us-east-2.rds.amazonaws.com:5432/projecttwodb";
	private static final String USERNAME = "projecttwo";
	private static final String PASSWORD = "Password";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
}
