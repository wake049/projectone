package com.dao;

import java.time.LocalDate;
import java.util.List;

import com.model.Reimbursment;

public interface ReimbursmentDAO {
	
	List<Reimbursment> getAll();
	List<Reimbursment> getAllByUserId(int id);
	void changeStatus(int i, int id, int status);
	void createReimb(Reimbursment remib, int id);
	List<Reimbursment> average();
	List<Reimbursment> sum();
	Reimbursment min();
	Reimbursment max();
}
