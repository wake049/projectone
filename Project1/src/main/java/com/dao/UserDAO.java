package com.dao;

import java.util.List;

import com.model.Users;

public interface UserDAO {
	
	Users getUser();
	List<Users> getAllUsers();
	Users getByUsername(String name);
}
