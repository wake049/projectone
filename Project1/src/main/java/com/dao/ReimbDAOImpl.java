package com.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.model.Reimbursment;
import com.model.Users;

public class ReimbDAOImpl implements ReimbursmentDAO{
	
	private ProjectTwoConnection ptc;
	
	public ReimbDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	

	public ReimbDAOImpl(ProjectTwoConnection ptc) {
		super();
		this.ptc = ptc;
	}



	public List<Reimbursment> getAll() {
		try(Connection con = ptc.getDBConnection()){
			String sql = "select r.remib_id, u.firstname, u.lastname, u1.firstname, u1.lastname, r.remib_amount, r.remib_submitted, r.remib_resolved, r.remib_description, r2.remib_status ,r3.remib_type from reimbursment r inner join users u on r.remib_author = u.userId full outer join users u1 on r.remib_resolver = u1.userid inner join remibstatus r2 on r.remib_statusid  = r2.remib_statusid inner join \r\n"
					+ "remibtype r3 on r.remib_typeid = r3.remibtypeid order by r.remib_id desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Reimbursment> remib = new ArrayList<>();
			while(rs.next()) {
				remib.add(new Reimbursment(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getDouble(6), rs.getObject(7, LocalDate.class), rs.getObject(8, LocalDate.class), 
						rs.getString(9), rs.getString(10), rs.getString(11)));
			}
			return remib;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<Reimbursment> getAllByUserId(int id) {
		try(Connection con = ptc.getDBConnection()){
			String sql = "select u1.firstname, u1.lastname, r.remib_amount, r.remib_submitted, r.remib_resolved, r.remib_description, r2.remib_status , r3.remib_type from reimbursment r full outer join  users u1 on r.remib_resolver = u1.userid \r\n"
					+ "inner join remibstatus r2 on r.remib_statusid  = r2.remib_statusid inner join  remibtype r3 on r.remib_typeid = r3.remibtypeid where remib_author = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			List<Reimbursment> remib = new ArrayList<>();
				while(rs.next()) {
					remib.add(new Reimbursment(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getObject(4, LocalDate.class), rs.getObject(5, LocalDate.class), 
							rs.getString(6), rs.getString(7), rs.getString(8)));
				}
			
			return remib;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void changeStatus(int i, int id, int status) {
		Reimbursment r =new Reimbursment();
		
		try(Connection con = ptc.getDBConnection()){
			String sql = "update reimbursment set remib_resolved = ?, remib_statusid = ?, remib_resolver = ? where remib_id = ?";
			CallableStatement cs = con.prepareCall(sql);
			r.setDateResolved(LocalDate.now());
			cs.setObject(1, LocalDate.now());
			cs.setInt(2, status);
			cs.setInt(3, i);
			cs.setInt(4, id);
			cs.execute();
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
	}


	@Override
	public void createReimb(Reimbursment remib, int id) {
		// TODO Auto-generated method stub
		try(Connection con = ptc.getDBConnection()){
			String sql = "select insert_remibless(?,?,?,?,?,?)";
			CallableStatement cs =con.prepareCall(sql);
			cs.setBigDecimal(1, BigDecimal.valueOf(remib.getAmount()));
			cs.setObject(2, LocalDate.now());
			cs.setString(3, remib.getDescription());
			cs.setInt(4, id);
			cs.setInt(5, 1);
			cs.setInt(6, Integer.parseInt(remib.getType()));
			cs.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	@Override
	public List<Reimbursment> average() {
		try(Connection con = ptc.getDBConnection()){
			String sql = "select u.firstname,  avg(remib_amount) from reimbursment inner join users u on remib_author = u.userid group by u.firstname";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Reimbursment> remib = new ArrayList<>();
				while(rs.next()) {
					remib.add(new Reimbursment(rs.getString(1), rs.getDouble(2)));
					} 
				return remib;
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public List<Reimbursment> sum() {
		try(Connection con = ptc.getDBConnection()){
			String sql = "select u.firstname, sum(remib_amount) from reimbursment r inner join users u on remib_author = u.userid group by u.firstname;";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Reimbursment> remib = new ArrayList<>();
				while(rs.next()) {
					remib.add(new Reimbursment(rs.getString(1), rs.getDouble(2)));
					} 
				return remib;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Reimbursment min() {
		Reimbursment remib = new Reimbursment();
		try(Connection con = ptc.getDBConnection()){
			String sql = "select minimum1.firstname, min(minimum1.total) totalmin \r\n"
					+ "from  (select u2.firstname, sum(remib_amount) as total\r\n"
					+ "from reimbursment r inner join users u2 on r.remib_author = u2.userId \r\n"
					+ "group by u2.firstname) minimum1\r\n"
					+ "group by minimum1.firstname order by totalmin asc limit 1;";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					remib = new Reimbursment(rs.getString(1), rs.getDouble(2));
					} 
				return remib;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Reimbursment max() {
		Reimbursment remib = new Reimbursment();
		try(Connection con = ptc.getDBConnection()){
			String sql = "select minimum1.firstname, max(minimum1.total) totalmax \r\n"
					+ "from  (select u2.firstname, sum(remib_amount) as total\r\n"
					+ "from reimbursment r inner join users u2 on r.remib_author = u2.userId \r\n"
					+ "group by u2.firstname) minimum1\r\n"
					+ "group by minimum1.firstname order by totalmax desc limit 1;\r\n"
					+ "";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					remib = new Reimbursment(rs.getString(1), rs.getDouble(2));
					} 
				return remib;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	
	

}
