package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Users;

public class UserDAOImpl implements UserDAO {
	
	private ProjectTwoConnection ptc;
	
	public UserDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	public UserDAOImpl(ProjectTwoConnection ptc) {
		super();
		this.ptc = ptc;
	}

	public Users getUser() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Users> getAllUsers() {                                                                               
		try(Connection con = ptc.getDBConnection()){
			String sql = "select * from users";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Users> useList = new ArrayList<>() ;
			while (rs.next()) {
				useList.add(new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7)));
			}
			return useList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Users getByUsername(String name) {
		try(Connection con = ptc.getDBConnection()){
			String sql = "select  * from users where username = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,name);
			ResultSet rs = ps.executeQuery();
			Users use = new Users();
			while(rs.next()) {
				use = new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
			}
			
			return use;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
