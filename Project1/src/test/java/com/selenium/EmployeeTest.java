package com.selenium;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeTest {


	private ManagerSel page;
	private static WebDriver driver;
	
	@BeforeAll
	public static void setupBeforeClass() {
		String filePath = "src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}
	
	@AfterAll
	public static void tearDownAfterClass() {
		driver.quit();
	}
	
	@BeforeEach
	public void setUp() {
		this.page = new ManagerSel(driver);
	}
	
	
	@Test
	public void testLoginHeader() {
		assertEquals(page.getHeader(), "Welcome to reimbursement website for company A");
	}
	
	@Test
	public void testLoginSuccess() {
		page.setUsername("employee");
		page.setPassword("employee");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("h1")));
		assertEquals("Your Reimbursements", driver.findElement(By.tagName("h1")).getText());
	}
	
	@Test
	public void testLoginFailure() {
		page.setUsername("All For One");
		page.setPassword("All For One");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/badlogin.html"));
		assertEquals("http://localhost:9028/html/badlogin.html", driver.getCurrentUrl());
	}
	@Test
	public void testCreateView() {
		page.setUsername("employee");
		page.setPassword("employee");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/employeepage.html"));
		WebElement  button = driver.findElement(By.xpath("/html/body/nav/div/div/ul/li[2]/a"));
		button.click();
		wait.until(ExpectedConditions.urlMatches("/employeecreate.html"));
		assertEquals("http://localhost:9028/html/employeecreate.html", driver.getCurrentUrl());
	}
	@Test
	public void testCreate() {
		page.setUsername("employee");
		page.setPassword("employee");
		page.submit();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/employeepage.html"));
		WebElement  button = driver.findElement(By.xpath("/html/body/nav/div/div/ul/li[2]/a"));
		button.click();
		wait.until(ExpectedConditions.urlMatches("/employeecreate.html"));
		WebElement amount = driver.findElement(By.name("amount"));
		WebElement des = driver.findElement(By.name("description"));
		WebElement date = driver.findElement(By.name("date"));
		WebElement select = driver.findElement(By.name("type"));
		WebElement submit = driver.findElement(By.name("btn"));
		Select dropdown = new Select(select);
		dropdown.selectByIndex(2);
		amount.sendKeys("10201");
		des.sendKeys("aklshgd");
		date.sendKeys("01/01/2022");
		dropdown.selectByIndex(1);
		submit.click();
		WebElement homePage = driver.findElement(By.xpath("/html/body/nav/div/div/ul/li[1]/a"));
		homePage.click();
		wait.until(ExpectedConditions.urlMatches("/employeepage.html"));
		assertEquals("10201", driver.findElement(By.xpath("/html/body/table/tbody/tr[5]/td[3]")).getText());
	}
}
