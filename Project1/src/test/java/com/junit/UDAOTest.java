package com.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dao.ProjectTwoConnection;
import com.dao.UserDAOImpl;
import com.model.Users;


public class UDAOTest {
	
	

@Mock
private ProjectTwoConnection ptc;

@Mock
private Connection con;

@Mock
private PreparedStatement ps;

@Mock
private CallableStatement cs;

@Mock
private ResultSet rs;

private UserDAOImpl uDao;

private Users use;
private List<Users> useList;

	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		uDao = new UserDAOImpl(ptc);
		when(ptc.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn("Test Success");
		use = new Users(1, "wake", "1234", "Tyler", "Allen", "wakebrdtyler@yahoo.com", 1);
		useList = Arrays.asList(use);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(use.getUserId());
		when(rs.getString(2)).thenReturn(use.getUsername());
		when(rs.getString(3)).thenReturn(use.getPassword());
		when(rs.getString(4)).thenReturn(use.getFirstname());
		when(rs.getString(5)).thenReturn(use.getLastname());
		when(rs.getString(6)).thenReturn(use.getEmail());
		when(rs.getInt(7)).thenReturn(use.getUserRole());
	}
	
	@Test
	public void testGetAll() {
		List<Users> uList = uDao.getAllUsers();
		Users u1 = uList.get(0);
		Users u2 = useList.get(0);
		assertEquals(u1.getUserId(), u2.getUserId());
		assertEquals(u1.getUsername(), u2.getUsername());
		assertEquals(u1.getPassword(), u2.getPassword());
	}
	
	@Test
	public void testGetUsername() {
		Users us1 = uDao.getByUsername("wake");
		assertEquals(us1.getUsername(), use.getUsername());
	}
	
}
