package com.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dao.ProjectTwoConnection;
import com.dao.ReimbDAOImpl;
import com.model.Reimbursment;
import com.model.Users;

public class RDAOTest {

	@Mock
	private ProjectTwoConnection ptc;

	@Mock
	private Connection con;

	@Mock
	private PreparedStatement ps;

	@Mock
	private CallableStatement cs;

	@Mock
	private ResultSet rs;

	private ReimbDAOImpl rDao;

	private Reimbursment r;
	private Reimbursment rAmount;
	private Reimbursment rGet;
	private Users use;
	private List<Reimbursment> rList;
	private List<Reimbursment> rAMountList;
	private List<Reimbursment> rGetList;

	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		rDao = new ReimbDAOImpl(ptc);
		when(ptc.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn("Test Success");
		r = new Reimbursment(1, "kevin", "piercall", "tyler", "allen", 1001.01, LocalDate.now(), LocalDate.now(), "laksdjf", "pending", "1");
		rAmount = new Reimbursment("William", 12013);
		rGet = new Reimbursment("William", "Allen", 4641, LocalDate.now(), LocalDate.now(), "askjbd", "ashd", "kajsd");
		rList = Arrays.asList(r);
		rAMountList = Arrays.asList(rAmount);
		rGetList = Arrays.asList(rGet);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(r.getId());
		when(rs.getString(2)).thenReturn(r.getFirstname());
		when(rs.getString(3)).thenReturn(r.getLastname());
		when(rs.getString(4)).thenReturn(r.getFirstname1());
		when(rs.getString(5)).thenReturn(r.getLastname1());
		when(rs.getDouble(6)).thenReturn(r.getAmount());
		when(rs.getObject(7)).thenReturn(r.getDateSubmitted());
		when(rs.getObject(8)).thenReturn(r.getDateResolved());
		when(rs.getString(9)).thenReturn(r.getDescription());
		when(rs.getString(10)).thenReturn(r.getStatus());
		when(rs.getString(11)).thenReturn(r.getType());
		when(rs.getString(1)).thenReturn(rAmount.getFirstname());
		when(rs.getDouble(2)).thenReturn(rAmount.getAmount());
		when(rs.getString(1)).thenReturn(rGet.getFirstname1());
		when(rs.getString(2)).thenReturn(rGet.getLastname1());
		when(rs.getDouble(3)).thenReturn(rGet.getAmount());
		when(rs.getObject(4)).thenReturn(rGet.getDateSubmitted());
		when(rs.getObject(5)).thenReturn(rGet.getDateResolved());
		when(rs.getString(6)).thenReturn(rGet.getDescription());
		when(rs.getString(7)).thenReturn(rGet.getStatus());
		when(rs.getString(8)).thenReturn(rGet.getType());
	}

	@Test
	public void testGetAll() {
		List<Reimbursment> r1 = rDao.getAll();
		Reimbursment rM = r1.get(0);
		Reimbursment rM1 = rList.get(0);
		assertEquals(rM.getAmount(), rM1.getAmount());
	}

	@Test
	public void testgetAllByUserId() {
		List<Reimbursment> r1 = rDao.getAllByUserId(1);
		Reimbursment rM = r1.get(0);
		System.out.println(rM.getId());
		Reimbursment rM1 = rGetList.get(0);
		assertEquals(rM.getAmount(), rM1.getAmount());
	}
	@Test
	public void testChangeStatus() throws SQLException {
		rDao.changeStatus(1, 1, 4);
		verify(cs, times(1)).execute();
	}
	@Test
	public void testInsertReimb() throws SQLException {
		rDao.createReimb(r, 1);
		verify(cs, times(1)).execute();
	}
	@Test
	public void testAvg() {
		List<Reimbursment> r1 = rDao.average();
		Reimbursment rM = r1.get(0);
		Reimbursment rM1 = rAMountList.get(0);
		assertEquals(rM.getAmount(), rM1.getAmount());
		assertEquals(rM.getFirstname(), rM1.getFirstname());
	}
	@Test
	public void testSum() {
		List<Reimbursment> r1 = rDao.sum();
		Reimbursment rM = r1.get(0);
		Reimbursment rM1 = rAMountList.get(0);
		assertEquals(rM.getAmount(), rM1.getAmount());
		assertEquals(rM.getFirstname(), rM1.getFirstname());
	}
	@Test
	public void testMin() {
		Reimbursment r1 = rDao.min();
		assertEquals(r1.getAmount(), rAmount.getAmount());
		assertEquals(r1.getFirstname(), rAmount.getFirstname());
	}
	@Test
	public void testMax() {
		Reimbursment r1 = rDao.max();
		assertEquals(r1.getAmount(), rAmount.getAmount());
		assertEquals(r1.getFirstname(), rAmount.getFirstname());
	}

}
