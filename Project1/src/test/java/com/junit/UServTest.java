package com.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.dao.UserDAOImpl;
import com.model.Users;
import com.service.UserService;

public class UServTest {

	@Mock
	private UserDAOImpl uDao;
	
	private UserService uServ;
	private Users use;
	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		uServ = new UserService(uDao);
		use = new Users(1, "alkjsd", "akshdd", "kjashdlk", "kjshadl", "kajshdg", 1);
		when(uDao.getByUsername(isA(String.class))).thenReturn(use);
	}
	
	@Test
	public void testUsernameAndPassword() {
		String useName = "alkjsd";
		String pass = "akshdd";
		Users use1 = uServ.verifyUsernameAndPassword(useName, pass);
		assertEquals(use1.getUserId(), use.getUserId());
	}
}
