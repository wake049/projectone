Feature: Approve or deny a reimbursement
  As a manager I would like to approve or deny a reimbursment


  Scenario: Manager can approve or deny
    Given user role is a manager
    And on the reimbursement page
    When a reimbursement status is pending
    Then I can change it to approve or deny
    
   
  Scenario: Manager can approve or deny
    Given user role is a manager
    And on the reimbusment page
    When a reimbursement status is approved
    Then I change not it to approve or deny