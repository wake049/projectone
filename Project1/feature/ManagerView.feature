Feature: Manager viewing all reimbursements
	As a manager I would like to view all reimbursements
	
Scenario:Manager view all reimbursements
	Given a manager is on the reimbursement page
	and a manager is logged in
	Then all reimbursements should show
	
Scenario:Manager view all reimbursements failed
	Given a manager is on the reimbursement page
	And a employee is logged in
	Then only that employees reimbursements should show