Feature: Employee logging in to ers app
	As a Employee, I wish to login to the ers app using the proper credentials


Scenario: Successful login to the ERS App
	Given a user is on at the login page
	When a user inputs their user name
	And a user inputs their password
	And a user submits the information
	But that user is of role Employee
	Then the user is redirected to the Manager ERS page
	
Scenario: Successful Failed to the ERS App
	Given a user is on at the login page
	When a user inputs their user name
	And a user inputs their password
	And a user submits the information
	But that user is of role Employee
	Then the user is redirected to the Manager ERS page